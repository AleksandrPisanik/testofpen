import java.io.File;
import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {

        String separator = File.separator;
        String path1 = ".."+separator+"Maven1"+separator+"src"+separator+"main"+separator+"resources"+separator+"firstString";
        File file1 = new File(path1);
        String path2 = ".."+separator+"Maven1"+separator+"src"+separator+"main"+separator+"resources"+separator+"secondTask";
        File file2 = new File(path2);

        FileRead.task1(file1);
        System.out.println();
        System.out.println();
        FileRead.task2(file2);

        System.out.println();
        System.out.println(System.getProperty("AUTHOR"));
        System.out.println();
    }
}
