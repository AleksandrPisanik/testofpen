import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

class FileRead {

    public static void task1(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);

        String str1 = scanner.nextLine();
        System.out.println("String 1: " + str1);
        String str2 = scanner.nextLine();
        System.out.println("String 2: " + str2);
        System.out.print("Result: ");

        String[] string1 = str1.split("\\s");
        String[] string2 = str2.split("\\s");

        int count = 0;

        for (int i = 0; i < string1.length; i++) {
            count = 0;
            for (int j = 0; j < string2.length; j++) {
                if (string1[i].equals(string2[j])) {
                    count++;
                }
            }
            if (count == 0) {
                System.out.print(string1[i] + " ");
            }
        }

    }

    public static void task2(File file) throws FileNotFoundException {
        Scanner scanner = new Scanner(file);

        String str1 = scanner.nextLine();
        System.out.println("String 1: " + str1);
        String str2 = scanner.nextLine();
        System.out.println("Word: " + str2);

        String [] string1 = str1.split("[,;:.!?\\s]+");
        String [] word = str2.split("\\s");
        char [] mas1 = str1.toCharArray();

        int count = 0;
        int repeator = 0;

        for (String words: string1 ) {
            if (words.equals(word[0])){
                ++count;
            }
        }
        System.out.println(count);

        for (int i=0;i<mas1.length;i++) {
            if (mas1[i]== '.' || mas1[i] == ',' ){
                repeator++;
            }
        }
        System.out.println("Znakov: " + repeator);
    }

}
